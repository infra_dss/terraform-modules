output "server-db-ip" {
  description = "Ip nginx"
  value = element(concat(vscale_server.*.nginx.public_address.address, [""]), 0)
}

# output "server-nginx-ip" {
#   description = "Ip db"
#   value       = vscale_server.nginx.public_address.address
# }

