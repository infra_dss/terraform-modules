resource "vscale_server" "nginx" {
  name     = var.srv_name
  keys     = var.key_name
  image    = var.instance_image
  location = var.location
  rplan    = var.instance_type
  # count    = 1

# provisioner "remote-exec" {
#     inline = ["sudo apt -y install python3"]

#     connection {
#       host        = "${self.public_address.address}"
#       type        = "ssh"
#       user        = "root"
#       private_key = file(pathexpand("~/.ssh/id_rsa"))
#     }
#   }

# provisioner "local-exec" {
#     command = "cd ../ansible && ansible-playbook -u root --vault-password-file=~/.ansible_vault_pass -i '${self.public_address.address},' --private-key ~/.ssh/id_rsa nginx.yaml"
# }
}
