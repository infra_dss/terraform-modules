variable "location" {
  description = "location"
  type        = string
  default     = "msk0"
}

variable "instance_type" {
  description = "Enter Instance Type"
  type        = string
  default     = "small"
}

variable "key_name" {
  description = "SSH key for server"
  type        = list
  default     = ["wsl", "home"]
}

variable "instance_image" {
  description = "Enter Instance image"
  type        = string
  default     = "ubuntu_20.04_64_001_master"
}

variable "srv_name" {
  description = "srv_name"
  type        = string
  default     = "prom"
}
